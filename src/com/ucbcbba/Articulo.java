package com.ucbcbba;

import java.util.List;

public class Articulo {
    private String titulo;
    private String texto;
    private int likes;
    private List<Comentario> comentarios;

    public Articulo(String titulo, String texto) {
        this.likes = 0;
        this.texto = texto;
        this.titulo=titulo;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getTexto() {
        return texto;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public List<Comentario> getComentarios() {
        return comentarios;
    }

    public void setComentarios(List<Comentario> comentarios) {
        this.comentarios = comentarios;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
}
