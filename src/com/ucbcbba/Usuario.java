package com.ucbcbba;

public class Usuario {
    private String nombre;
    private String email;
    private int telefono;
    private String direc;


    public String getNombre() {
        return nombre;
    }

    public String getDirec() {
        return direc;
    }

    public int getTelefono() {
        return telefono;
    }

    public String getEmail() {
        return email;
    }

    public Usuario(String nombre,int telefono,String direc,String email) {
        this.nombre = nombre;
        this.direc = direc;
        this.email = email;
        this.telefono = telefono;
    }

}
